package com.sql.application.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sql.application.main.InvoicePriceCalculator;

public class InvoicePriceCalculatorTest {
	
	// The InvoicePriceCalculator
	private InvoicePriceCalculator invoicePriceCalculator;
	
	/*
	 * Test initializer
	 */
	@BeforeTest
	public void before() {
		invoicePriceCalculator = new InvoicePriceCalculator();
	}
	
	/*
	 * Test Total Amount Of Invoices By Id For Given Customer Has Valid Invoice
	 */
	@Test
	public void testTotalAmountOfInvoicesByIdForGivenCustomerHasValidInvoice() throws Exception {
		int customerId = 40;
		double total = invoicePriceCalculator.totalAmountOfInvoicesById(String.valueOf(customerId));
		assertEquals(total, 6675.0);
	}
	
	/*
	 * Test Total Amount Of Invoices By Id For Given Customer Has No Valid Invoice
	 */
	@Test
	public void testTotalAmountOfInvoicesByIdForGivenCustomerHasNoInvoice() throws Exception {
		int customerId = 49;
		double total = invoicePriceCalculator.totalAmountOfInvoicesById(String.valueOf(customerId));
		assertEquals(total, 0);
	}
	
	/*
	 * Test Total Amount Of Invoices By Id For Given Customer Id Is Invalid
	 */
	@Test
	public void testTotalAmountOfInvoicesByIdForGivenCustomerIdIsInvalid() throws Exception {
		int customerId = -1;
		double total = invoicePriceCalculator.totalAmountOfInvoicesById(String.valueOf(customerId));
		assertEquals(total, 0);
	}
	
	/*
	 * Test tear down
	 */
	@AfterTest
	public void after() {
		invoicePriceCalculator = null;
	}
}

package com.sql.application.util;

import java.util.List;
/*
 * Application Util class
 */
public class AppUtil {
	public static double totalCosts(List<String> costs) {
		double sum = 0;
		for(String cost : costs) {
			sum = sum+Double.parseDouble(cost);
		}
		return sum; 
	}
}
